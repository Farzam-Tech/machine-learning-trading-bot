# Machine Learning Trading Bot 

I have combined new algorithmic trading skills in financial Python programming and machine learning to create an algorithmic trading bot that learns and adapts to new data and evolving markets.

In a Jupyter notebook: 

* Implement an algorithmic trading strategy that uses machine learning to automate the trade decisions.

* Adjust the input parameters to optimize the trading algorithm.

* Train a new machine learning model and compare its performance to that of a baseline model.

### Instructions

* Establish a Baseline Performance

* Tune the Baseline Trading Algorithm

* Evaluate a New Machine Learning Classifier

* Create an Evaluation Report

#### Establish a Baseline Performance


In this section, I have established a baseline performance for the trading algorithm. To do so, completed the following steps.


1. Import the OHLCV dataset into a Pandas DataFrame.

2. Generate trading signals using short- and long-window SMA values.

3. Split the data into training and testing datasets.

4. Use the `SVC` classifier model from SKLearn's support vector machine (SVM) learning method to fit the training data and make predictions based on the testing data.

5. Review the classification report associated with the `SVC` model predictions.

6. Create a predictions DataFrame that contains columns for “Predicted” values, “Actual Returns”, and “Strategy Returns”.

7. Create a cumulative return plot that shows the actual returns vs. the strategy returns.


#### Tune the Baseline Trading Algorithm

In this section, I have tuned, or adjusted, the model’s input features to find the parameters that result in the best trading outcomes. To do so, completed the following steps:

1. Tune the training algorithm by adjusting the size of the training dataset. To do so, slice our data into different periods.

2. Tune the trading algorithm by adjusting the SMA input features. 

3. Choose the set of parameters that best improved the trading algorithm returns.

#### Evaluate a New Machine Learning Classifier

1. Import a new classifier, such as `AdaBoost`, `DecisionTreeClassifier`, or `LogisticRegression`. 

2. Using the original training data as the baseline model, fit another model with the new classifier.

3. Backtest the new model to evaluate its performance. 
